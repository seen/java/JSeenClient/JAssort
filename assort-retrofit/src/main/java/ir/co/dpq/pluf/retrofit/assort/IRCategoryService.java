package ir.co.dpq.pluf.retrofit.assort;

import java.util.Map;

import ir.co.dpq.pluf.assort.PCategory;
import ir.co.dpq.pluf.retrofit.RPaginatorPage2;
import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.QueryMap;

public interface IRCategoryService {

	@FormUrlEncoded
	@POST("/api/assort/category/new")
	PCategory create(@FieldMap Map<String, Object> map);

	@FormUrlEncoded
	@POST("/api/assort/category/new")
	void create(@FieldMap Map<String, Object> map, Callback<PCategory> callback);

	@GET("/api/assort/category/{id}")
	PCategory get(@Path("id") long id);

	@GET("/api/assort/category/{id}")
	void get(@Path("id") long id, Callback<PCategory> callback);

	@FormUrlEncoded
	@POST("/api/assort/category/{id}")
	PCategory update(@Path("id") long id, @FieldMap Map<String, Object> map);

	@FormUrlEncoded
	@POST("/api/assort/category/{id}")
	void update(@Path("id") long id, @FieldMap Map<String, Object> map, Callback<PCategory> callback);

	@DELETE("/api/assort/category/{id}")
	PCategory delete(@Path("id") long id);

	@DELETE("/api/assort/category/{id}")
	void delete(@Path("id") long id, Callback<PCategory> callback);

	@GET("/api/assort/category/find")
	RPaginatorPage2<PCategory> find(@QueryMap Map<String, Object> params);

	@GET("/api/assort/category/find")
	void find(@QueryMap Map<String, Object> params, Callback<RPaginatorPage2<PCategory>> callback);
	
}
