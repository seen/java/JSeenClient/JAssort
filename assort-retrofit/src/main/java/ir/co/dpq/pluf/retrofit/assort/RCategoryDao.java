package ir.co.dpq.pluf.retrofit.assort;

import java.util.HashMap;
import java.util.Map;

import ir.co.dpq.pluf.IPCallback;
import ir.co.dpq.pluf.IPPaginatorPage;
import ir.co.dpq.pluf.PException;
import ir.co.dpq.pluf.PPaginatorParameter;
import ir.co.dpq.pluf.assort.IPCategoryDao;
import ir.co.dpq.pluf.assort.PCategory;
import ir.co.dpq.pluf.retrofit.RPaginatorPage2;
import ir.co.dpq.pluf.retrofit.Util;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RCategoryDao implements IPCategoryDao {

	private IRCategoryService categoryService;

	public IRCategoryService getRService() {
		return categoryService;
	}

	public void setRService(IRCategoryService categoryService) {
		this.categoryService = categoryService;
	}

	/**
	 * Wraps an {@link IPCallback} to a {@link Callback} and return wrapped
	 * callback
	 * 
	 * @param callback
	 *            {@link IPCallback}
	 * @return {@link Callback}
	 */
	private static Callback<PCategory> wrapToCallback(final IPCallback<PCategory> callback) {
		Callback<PCategory> cb = new Callback<PCategory>() {
			public void success(PCategory t, Response response) {
				callback.success(t);
			}

			public void failure(RetrofitError error) {
				callback.failure(new PException(error.getMessage(), error));
			}
		};
		return cb;
	}

	public void create(PCategory category, IPCallback<PCategory> callback) {
		categoryService.create(category.toMap(), wrapToCallback(callback));
	}

	public void get(long id, IPCallback<PCategory> callback) {
		categoryService.get(id, wrapToCallback(callback));
	}

	public void update(PCategory category, IPCallback<PCategory> callback) {
		categoryService.update(category.getId(), category.toMap(), wrapToCallback(callback));
	}

	public void delete(long id, IPCallback<PCategory> callback) {
		categoryService.delete(id, wrapToCallback(callback));
	}

	public void find(PPaginatorParameter param, final IPCallback<IPPaginatorPage<PCategory>> callback) {
		Map<String, Object> paramMap;
		if (param == null) {
			paramMap = new HashMap<String, Object>();
		} else {
			paramMap = Util.toRObject(param).toMap();
		}
		categoryService.find(paramMap, new Callback<RPaginatorPage2<PCategory>>() {

			public void success(RPaginatorPage2<PCategory> result, Response response) {
				callback.success(result);
			}

			public void failure(RetrofitError error) {
				callback.failure(new PException(error.getMessage(), error));
			}
		});
	}

	public PCategory create(PCategory category) {
		return categoryService.create(category.toMap());
	}

	public PCategory get(long id) {
		return categoryService.get(id);
	}

	public PCategory update(PCategory category) {
		return categoryService.update(category.getId(), category.toMap());
	}

	public PCategory delete(long id) {
		return categoryService.delete(id);
	}

	public IPPaginatorPage<PCategory> find(PPaginatorParameter param) {
		Map<String, Object> paramMap;
		if (param == null) {
			paramMap = new HashMap<String, Object>();
		} else {
			paramMap = Util.toRObject(param).toMap();
		}
		return categoryService.find(paramMap);
	}

	public void create(long parentId, PCategory category, IPCallback<PCategory> callback) {
		Map<String, Object> map = category.toMap();
		map.put("parent", parentId);
		categoryService.create(map, wrapToCallback(callback));
	}

	public PCategory create(long parentId, PCategory category) {
		Map<String, Object> map = category.toMap();
		map.put("parent", parentId);
		return categoryService.create(map);
	}

}
