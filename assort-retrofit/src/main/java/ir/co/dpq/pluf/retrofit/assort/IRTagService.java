package ir.co.dpq.pluf.retrofit.assort;

import java.util.Map;

import ir.co.dpq.pluf.assort.PTag;
import ir.co.dpq.pluf.retrofit.RPaginatorPage2;
import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.QueryMap;

public interface IRTagService {

	@FormUrlEncoded
	@POST("/api/assort/tag/new")
	PTag create(@FieldMap Map<String, Object> map);

	@FormUrlEncoded
	@POST("/api/assort/tag/new")
	void create(@FieldMap Map<String, Object> map, Callback<PTag> callback);

	@GET("/api/assort/tag/{id}")
	PTag get(@Path("id") long id);

	@GET("/api/assort/tag/{id}")
	void get(@Path("id") long id, Callback<PTag> callback);
	
	@GET("/api/assort/tag/{name}")
	PTag get(@Path("name") String name);
	
	@GET("/api/assort/tag/{name}")
	void get(@Path("name") String name, Callback<PTag> callback);

	@FormUrlEncoded
	@POST("/api/assort/tag/{id}")
	PTag update(@Path("id") long id, @FieldMap Map<String, Object> map);

	@FormUrlEncoded
	@POST("/api/assort/tag/{id}")
	void update(@Path("id") long id, @FieldMap Map<String, Object> map, Callback<PTag> callback);

	@DELETE("/api/assort/tag/{id}")
	PTag delete(@Path("id") long id);

	@DELETE("/api/assort/tag/{id}")
	void delete(@Path("id") long id, Callback<PTag> callback);

	@GET("/api/assort/tag/find")
	RPaginatorPage2<PTag> find(@QueryMap Map<String, Object> params);

	@GET("/api/assort/tag/find")
	void find(@QueryMap Map<String, Object> params, Callback<RPaginatorPage2<PTag>> callback);
	
}
