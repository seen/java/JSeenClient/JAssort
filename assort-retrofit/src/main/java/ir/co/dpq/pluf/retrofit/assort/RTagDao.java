package ir.co.dpq.pluf.retrofit.assort;

import java.util.HashMap;
import java.util.Map;

import ir.co.dpq.pluf.IPCallback;
import ir.co.dpq.pluf.IPPaginatorPage;
import ir.co.dpq.pluf.PException;
import ir.co.dpq.pluf.PPaginatorParameter;
import ir.co.dpq.pluf.assort.IPTagDao;
import ir.co.dpq.pluf.assort.PTag;
import ir.co.dpq.pluf.retrofit.RPaginatorPage2;
import ir.co.dpq.pluf.retrofit.Util;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RTagDao implements IPTagDao {

	private IRTagService tagService;

	public IRTagService getRService() {
		return tagService;
	}

	public void setRService(IRTagService tagService) {
		this.tagService = tagService;
	}

	/**
	 * Wraps an {@link IPCallback} to a {@link Callback} and return wrapped
	 * callback
	 * 
	 * @param callback
	 *            {@link IPCallback}
	 * @return {@link Callback}
	 */
	private static Callback<PTag> wrapToCallback(final IPCallback<PTag> callback) {
		Callback<PTag> cb = new Callback<PTag>() {
			public void success(PTag t, Response response) {
				callback.success(t);
			}

			public void failure(RetrofitError error) {
				callback.failure(new PException(error.getMessage(), error));
			}
		};
		return cb;
	}

	public void create(PTag tag, IPCallback<PTag> callback) {
		tagService.create(tag.toMap(), wrapToCallback(callback));
	}

	public void get(long id, IPCallback<PTag> callback) {
		tagService.get(id, wrapToCallback(callback));
	}

	public void getByName(String name, IPCallback<PTag> callback) {
		tagService.get(name, wrapToCallback(callback));
	}
	
	public void update(PTag tag, IPCallback<PTag> callback) {
		tagService.update(tag.getId(), tag.toMap(), wrapToCallback(callback));
	}

	public void delete(long id, IPCallback<PTag> callback) {
		tagService.delete(id, wrapToCallback(callback));
	}

	public void find(PPaginatorParameter param, final IPCallback<IPPaginatorPage<PTag>> callback) {
		Map<String, Object> paramMap;
		if (param == null) {
			paramMap = new HashMap<String, Object>();
		} else {
			paramMap = Util.toRObject(param).toMap();
		}
		tagService.find(paramMap, new Callback<RPaginatorPage2<PTag>>() {

			public void success(RPaginatorPage2<PTag> result, Response response) {
				callback.success(result);
			}

			public void failure(RetrofitError error) {
				callback.failure(new PException(error.getMessage(), error));
			}
		});
	}

	public PTag create(PTag tag) {
		return tagService.create(tag.toMap());
	}

	public PTag get(long id) {
		return tagService.get(id);
	}

	public PTag update(PTag tag) {
		return tagService.update(tag.getId(), tag.toMap());
	}

	public PTag delete(long id) {
		return tagService.delete(id);
	}

	public IPPaginatorPage<PTag> find(PPaginatorParameter param) {
		Map<String, Object> paramMap;
		if (param == null) {
			paramMap = new HashMap<String, Object>();
		} else {
			paramMap = Util.toRObject(param).toMap();
		}
		return tagService.find(paramMap);
	}

	public PTag getByName(String name) {
		return tagService.get(name);
	}

}
