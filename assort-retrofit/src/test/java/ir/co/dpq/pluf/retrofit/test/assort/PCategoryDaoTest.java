package ir.co.dpq.pluf.retrofit.test.assort;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ir.co.dpq.pluf.PUserDaoRetrofit;
import ir.co.dpq.pluf.assort.IPCategoryDao;
import ir.co.dpq.pluf.retrofit.PErrorHandler;
import ir.co.dpq.pluf.retrofit.assort.IRCategoryService;
import ir.co.dpq.pluf.retrofit.assort.RCategoryDao;
import ir.co.dpq.pluf.retrofit.user.IRUserService;
import ir.co.dpq.pluf.test.assort.AbstractPCategoryDaoTest;
import ir.co.dpq.pluf.test.assort.TestCoreConstant;
import ir.co.dpq.pluf.user.IPUserDao;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class PCategoryDaoTest extends AbstractPCategoryDaoTest{

	IPCategoryDao categoryDao;
	IPUserDao userDao;

	public PCategoryDaoTest() {
		CookieManager cookieManager = new CookieManager();
		cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		CookieHandler.setDefault(cookieManager);

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder//
				.setDateFormat("yyyy-MM-dd HH:mm:ss");//
		Gson gson = gsonBuilder.create();

		RestAdapter restAdapter = new RestAdapter.Builder()
				// تعیین مبدل داده
				.setConverter(new GsonConverter(gson))
				// تعیین کنترل کننده خطا
				.setErrorHandler(new PErrorHandler())
				// تعیین آدرس سایت مورد نظر
				.setEndpoint(TestCoreConstant.END_POINT)
				// ایجاد یک نمونه
				.build();

		// ایجاد سرویس‌ها
		IRCategoryService service = restAdapter.create(IRCategoryService.class);
		RCategoryDao dao = new RCategoryDao();
		dao.setRService(service);
		categoryDao = dao;
		
		IRUserService userSerivece = restAdapter.create(IRUserService.class);
		PUserDaoRetrofit usrDao = new PUserDaoRetrofit();
		usrDao.setUserService(userSerivece);
		userDao = usrDao;
	}
	
	@Override
	protected IPCategoryDao getPCategoryDaoInstance() {
		return categoryDao;
	}

	@Override
	protected IPUserDao getUserDaoInstance() {
		return userDao;
	}

}
