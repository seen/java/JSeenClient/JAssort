package ir.co.dpq.pluf.retrofit.test.assort;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ir.co.dpq.pluf.PUserDaoRetrofit;
import ir.co.dpq.pluf.assort.IPTagDao;
import ir.co.dpq.pluf.retrofit.PErrorHandler;
import ir.co.dpq.pluf.retrofit.assort.IRTagService;
import ir.co.dpq.pluf.retrofit.assort.RTagDao;
import ir.co.dpq.pluf.retrofit.user.IRUserService;
import ir.co.dpq.pluf.test.assort.AbstractPTagDaoTest;
import ir.co.dpq.pluf.test.assort.TestCoreConstant;
import ir.co.dpq.pluf.user.IPUserDao;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class PTagDaoTest extends AbstractPTagDaoTest{

	IPTagDao tagDao;
	IPUserDao userDao;

	public PTagDaoTest() {
		CookieManager cookieManager = new CookieManager();
		cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		CookieHandler.setDefault(cookieManager);

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder//
				.setDateFormat("yyyy-MM-dd HH:mm:ss");//
		Gson gson = gsonBuilder.create();

		RestAdapter restAdapter = new RestAdapter.Builder()
				// تعیین مبدل داده
				.setConverter(new GsonConverter(gson))
				// تعیین کنترل کننده خطا
				.setErrorHandler(new PErrorHandler())
				// تعیین آدرس سایت مورد نظر
				.setEndpoint(TestCoreConstant.END_POINT)
				// ایجاد یک نمونه
				.build();

		// ایجاد سرویس‌ها
		IRTagService service = restAdapter.create(IRTagService.class);
		RTagDao dao = new RTagDao();
		dao.setRService(service);
		tagDao = dao;
		
		IRUserService userSerivece = restAdapter.create(IRUserService.class);
		PUserDaoRetrofit usrDao = new PUserDaoRetrofit();
		usrDao.setUserService(userSerivece);
		userDao = usrDao;
	}
	
	@Override
	protected IPTagDao getPTagDaoInstance() {
		return tagDao;
	}

	@Override
	protected IPUserDao getUserDaoInstance() {
		return userDao;
	}

}
