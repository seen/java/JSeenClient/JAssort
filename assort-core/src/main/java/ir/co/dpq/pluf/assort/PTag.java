package ir.co.dpq.pluf.assort;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.google.gson.annotations.SerializedName;

@Entity(name = "tag")
@Table(name = "tag")
public class PTag implements Serializable {

	private static final long serialVersionUID = 1636521003254243406L;

	@Id
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "name", nullable = true)
	private String name;

	@Column(name = "description", nullable = true)
	private String description;

	@Column(name = "creation_dtime")
	@SerializedName("creation_dtime")
	@Temporal(TemporalType.DATE)
	private Date creation;

	@Column(name = "modif_dtime")
	@SerializedName("modif_dtime")
	@Temporal(TemporalType.DATE)
	private Date modification;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public Date getModification() {
		return modification;
	}

	public void setModification(Date modification) {
		this.modification = modification;
	}

	public Map<String, Object> toMap() {
		HashMap<String, Object> map = new HashMap<String, Object>();

		map.put("id", getId());
		map.put("name", getName());
		map.put("description", getDescription());

		return map;
	}

}
