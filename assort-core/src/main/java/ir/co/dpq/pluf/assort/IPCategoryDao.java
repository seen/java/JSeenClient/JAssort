package ir.co.dpq.pluf.assort;

import ir.co.dpq.pluf.IModelDao;
import ir.co.dpq.pluf.IPCallback;

public interface IPCategoryDao extends IModelDao<PCategory>{

	/**
	 * Create new category as child of a category given by id
	 * @param parentId id of parent category
	 * @param category category contain information about new category
	 * @param callback asynchronous response contain newly created category
	 */
	void create(long parentId, PCategory category, IPCallback<PCategory> callback);
	
	/**
	 * Create new category as child of a category given by id
	 * @param parentId id of parent category
	 * @param category category contain information about new category
	 * @return newly created category
	 */
	PCategory create(long parentId, PCategory category);
	
}
