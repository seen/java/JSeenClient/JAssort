package ir.co.dpq.pluf.assort;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.google.gson.annotations.SerializedName;

@Entity(name = "category")
@Table(name = "category")
public class PCategory implements Serializable {

	private static final long serialVersionUID = -271414542666560873L;

	@Id
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "name", nullable = true)
	private String name;

	@Column(name = "description", nullable = true)
	private String description;

	@Column(name = "creation_dtime")
	@SerializedName("creation_dtime")
	@Temporal(TemporalType.DATE)
	private Date creation;

	@Column(name = "modif_dtime")
	@SerializedName("modif_dtime")
	@Temporal(TemporalType.DATE)
	private Date modification;

	// @ManyToOne(fetch = FetchType.EAGER)
	// @JoinColumn(name = "parent")
	// private PCategory parent;
	//
	// @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
	// private List<PCategory> childs;
	//
	// @ManyToOne(fetch = FetchType.EAGER)
	// @JoinColumn(name = "content")
	// private PContent content;
	//
	// @ManyToOne(fetch = FetchType.EAGER)
	// @JoinColumn(name = "thumbnail")
	// private PContent thumbnail;

	@Column(nullable = true)
	private long parent;

	@Column(nullable = true)
	private long content;
	
	@Column(nullable = true)
	private long thumbnail;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public Date getModification() {
		return modification;
	}

	public void setModification(Date modification) {
		this.modification = modification;
	}

	public long getParent() {
		return parent;
	}

	public void setParent(long parent) {
		this.parent = parent;
	}

	public long getContent() {
		return content;
	}

	public void setContent(long content) {
		this.content = content;
	}

	public long getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(long thumbnail) {
		this.thumbnail = thumbnail;
	}

	public Map<String, Object> toMap(){
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("id", getId());
		map.put("name", getName());
		map.put("description", getDescription());
		map.put("parent", getParent());
		map.put("content", getContent());
		map.put("thumbnail", getThumbnail());
		
		return map;
	}
	
//	public PCategory getParent() {
//		return parent;
//	}
//
//	public void setParent(PCategory parent) {
//		this.parent = parent;
//	}
//
//	public List<PCategory> getChilds() {
//		return childs;
//	}
//
//	public void setChilds(List<PCategory> childs) {
//		this.childs = childs;
//	}
//
//	public PContent getContent() {
//		return content;
//	}
//
//	public void setContent(PContent content) {
//		this.content = content;
//	}
//
//	public PContent getThumbnail() {
//		return thumbnail;
//	}
//
//	public void setThumbnail(PContent thumbnail) {
//		this.thumbnail = thumbnail;
//	}
	
}
