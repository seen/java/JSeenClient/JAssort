package ir.co.dpq.pluf.assort;

import ir.co.dpq.pluf.IModelDao;
import ir.co.dpq.pluf.IPCallback;

public interface IPTagDao extends IModelDao<PTag>{

	void getByName(String name, IPCallback<PTag> callback);
	
	PTag getByName(String name);
}
