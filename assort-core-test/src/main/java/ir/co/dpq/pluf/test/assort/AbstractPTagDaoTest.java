package ir.co.dpq.pluf.test.assort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Random;

import org.junit.Test;

import ir.co.dpq.pluf.IModelDao;
import ir.co.dpq.pluf.assort.IPTagDao;
import ir.co.dpq.pluf.assort.PTag;
import ir.co.dpq.pluf.test.AbstractModelDaoTest;

public abstract class AbstractPTagDaoTest extends AbstractModelDaoTest<PTag> {

	private IPTagDao dao;
	
	@Override
	public void initTest() {
		super.initTest();
		dao = getPTagDaoInstance();
	}
	
	protected abstract IPTagDao getPTagDaoInstance();
	
	@Override
	protected IModelDao<PTag> getModelDaoInstance() {
		return getPTagDaoInstance();
	}
	
	@Override
	protected PTag getNewModel() {
		Random rnd = new Random(System.currentTimeMillis());
		int num = rnd.nextInt();
		PTag model = new PTag();
		model.setName("tag-" + num);
		model.setDescription("Description of tag-" + num);
		return model;
	}

	@Override
	protected void assertModel(PTag obj, PTag newObj) {
		assertEquals(obj.getName(), newObj.getName());
		assertEquals(obj.getDescription(), newObj.getDescription());
	}

	@Override
	protected long getId(PTag obj) {
		return obj.getId();
	}

	@Test
	public void getByNameTest(){
		PTag obj = getNewModel();
		PTag obj2 = dao.create(obj);
		assertNotNull(obj2);
		assertModel(obj, obj2);

		PTag obj3 = dao.getByName(obj2.getName());
		assertNotNull(obj3);
		assertModel(obj2, obj3);
		
		modelCache.add(obj2);
	}
	
}
