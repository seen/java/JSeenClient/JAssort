package ir.co.dpq.pluf.test.assort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Random;

import org.junit.Test;

import ir.co.dpq.pluf.IModelDao;
import ir.co.dpq.pluf.assort.IPCategoryDao;
import ir.co.dpq.pluf.assort.PCategory;
import ir.co.dpq.pluf.test.AbstractModelDaoTest;

public abstract class AbstractPCategoryDaoTest extends AbstractModelDaoTest<PCategory> {

	private IPCategoryDao dao;
	
	@Override
	public void initTest() {
		super.initTest();
		dao = getPCategoryDaoInstance();
	}
	
	protected abstract IPCategoryDao getPCategoryDaoInstance();
	
	@Override
	protected IModelDao<PCategory> getModelDaoInstance() {
		return getPCategoryDaoInstance();
	}
	
	@Override
	protected PCategory getNewModel() {
		Random rnd = new Random(System.currentTimeMillis());
		int num = rnd.nextInt();
		PCategory model = new PCategory();
		model.setName("category-" + num);
		model.setDescription("Description of category-" + num);
		model.setParent(rnd.nextInt(10));
		model.setContent(rnd.nextInt(10));
		model.setThumbnail(rnd.nextInt(10));
		return model;
	}

	@Override
	protected void assertModel(PCategory obj, PCategory newObj) {
		assertEquals(obj.getName(), newObj.getName());
		assertEquals(obj.getDescription(), newObj.getDescription());
		assertEquals(obj.getParent(), newObj.getParent());
		assertEquals(obj.getContent(), newObj.getContent());
		assertEquals(obj.getThumbnail(), newObj.getThumbnail());
	}

	@Override
	protected long getId(PCategory obj) {
		return obj.getId();
	}

	@Test
	public void createSubcategoryTest(){
		PCategory obj = getNewModel();
		PCategory obj2 = dao.create(1, obj);
		obj.setParent(1);
		assertNotNull(obj2);
		assertModel(obj, obj2);
		
		modelCache.add(obj2);
	}
	
}
